﻿# Autor : Darkristal (jbeltran.abreo@gmail.com)
# Description: Very simple app to suspend (to save charge (?) ) the pocket chip

import os
import signal
import json
import subprocess

from urllib2 import Request, urlopen, URLError

from gi.repository import Gtk as gtk
from gi.repository import AppIndicator3 as appindicator
from gi.repository import Notify as notify


APPINDICATOR_ID = 'pybrigthness'
brg=1

def main():
    indicator = appindicator.Indicator.new(APPINDICATOR_ID, os.path.abspath('suspend.svg'), appindicator.IndicatorCategory.SYSTEM_SERVICES)
    indicator.set_status(appindicator.IndicatorStatus.ACTIVE)
    indicator.set_menu(build_menu())
    notify.init(APPINDICATOR_ID)
    gtk.main()

def build_menu():
    menu = gtk.Menu()
    item_status = gtk.MenuItem('+')
    item_status.connect('activate', up)
    menu.append(item_status)
    item_status = gtk.MenuItem('-')
    item_status.connect('activate', down)
    menu.append(item_status)
    
    
    
    item_quit = gtk.MenuItem('Quit')
    item_quit.connect('activate', quit)
    menu.append(item_quit)
    menu.show_all()
    
    return menu

def fetch_status():
    status = subprocess.Popen(["echo 1 > /sys/devices/platform/backlight/backlight/backlight/brightness"], stdout=subprocess.PIPE).communicate()[0]    
    return status

def status(_):
    notify.Notification.new("Status", fetch_status(), None).show()
    
def up(_):
    global brg
    brg=brg+1
    os.system("echo " + str(brg) + " > /sys/devices/platform/backlight/backlight/backlight/brightness")   
    
def down(_):
    global brg
    brg=brg-1
    os.system("echo " + str(brg) + " > /sys/devices/platform/backlight/backlight/backlight/brightness")   

def quit(_):
    notify.uninit()
    gtk.main_quit()

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    main()
