# Autor : Darkristal (jbeltran.abreo@gmail.com)
# Description: Very simple app to show the battery status in the system tray, use sudo to run

import os
import signal
import json
import subprocess

from urllib2 import Request, urlopen, URLError

from gi.repository import Gtk as gtk
from gi.repository import AppIndicator3 as appindicator
from gi.repository import Notify as notify


APPINDICATOR_ID = 'battindicator'

def main():
    indicator = appindicator.Indicator.new(APPINDICATOR_ID, os.path.abspath('battery-half.svg'), appindicator.IndicatorCategory.SYSTEM_SERVICES)
    indicator.set_status(appindicator.IndicatorStatus.ACTIVE)
    indicator.set_menu(build_menu())
    notify.init(APPINDICATOR_ID)
    gtk.main()

def build_menu():
    menu = gtk.Menu()
    item_status = gtk.MenuItem('Status')
    item_status.connect('activate', status)
    menu.append(item_status)
    item_quit = gtk.MenuItem('Quit')
    item_quit.connect('activate', quit)
    menu.append(item_quit)
    menu.show_all()
    return menu

def fetch_status():
    status = subprocess.Popen(["battery.sh"], stdout=subprocess.PIPE).communicate()[0]    
    return status

def status(_):
    notify.Notification.new("Status", fetch_status(), None).show()

def quit(_):
    notify.uninit()
    gtk.main_quit()

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    main()
