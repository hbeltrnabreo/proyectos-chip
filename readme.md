**Utilidades pocket chip**

Este repositorio fue creado para hacer pequeñas utilidades que ayuden a facilitar el uso de algunas llamadas nativas que se encuentran en el pocket-home, pero no en el resto de escritorios como fluxbox o matchbox.

Los proyectos actualmente en curso son los siguientes:

**pybatt**
Pequeño script para mostrar el status de la bateria en la bandeja del sistema.

**pybrigthness**
script para controlar el brillo de la pantalla

**pysuspend**
script para mandar a dormir al chip, aun no se como apagar el brillo de la pantalla