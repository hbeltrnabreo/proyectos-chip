lo que tenemos:
el applet battapplet esta para las plataformas que lo soporten,
 permite ver la cantidad de bateria disponible

para controlar el brillo se usa lo siguiente:
echo 1~10 > /sys/devices/platform/backlight/backlight/backlight/brightness

donde los valores del 1 al 10 son asignados de acuerdo al brillo

para mandar a dormir
xset dpms force off

o

sleep 1 && xset dpms force off ## este comando manda a dormir, pero no apaga
la luz de fondo

usa este link para consultar como apagar los leds mientras duerme
https://bbs.nextthing.co/t/feature-request-waking-from-sleep-mode/6968/2


